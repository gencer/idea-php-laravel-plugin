package de.espend.idea.laravel;


import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class LaravelIcons {
    public static final Icon LARAVEL = IconLoader.getIcon("icons/laravel.png");
}
